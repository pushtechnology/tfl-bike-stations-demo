# README #

Application to show nearby London's bike to hire stations with real time status updates. It is a demonstration of use of a Diffusion technology provided by the Push Technology. It allows for the distribution of a real time content to the large audience.

### How do I get set up? ###

To use the application you need to install the Diffusion server and clone the project which consists 2 applications with built-in Diffusion clients:
* publisher
* subscriber

The project uses maven as a build tool and doesn't require an initial configuration if you want to run all components on the same machine. Otherwise you need to specify location of the Diffusion server.

More detailed description can be found at https://www.pushtechnology.com/?p=19750

Please contact mkruglik@pushtechnology.com if you require an additional information.