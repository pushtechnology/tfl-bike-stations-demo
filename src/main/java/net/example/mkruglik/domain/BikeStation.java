/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package net.example.mkruglik.domain;

import net.example.mkruglik.tflAPI.domain.BikeStationAPI;

/**
 * Class represents bike station with all details important from system point of view.
 * Normally it is used to rid of the unnecessary data provided by TfL API.
 * The class is immutable
 * @author mkruglik
 */
public class BikeStation implements Location, Station {
// Functionally class is final. However due to parser requirements it needs to provide
// default constructor and fields can not be final.
    private String id;
    private String name;
    private double latitude;
    private double longitude;
    private int bikesNumber;
    private int docksNumber;
    private int emtyDocksNumber;
    private String modified;

    /**
     *  Default constructor required by JSON parser. Visibility: protected
     */
    protected BikeStation() { }

    private BikeStation(BikeStationAPI bikeStationAPI, int bikesNumber) {
        this.id = bikeStationAPI.getId();
        this.name = bikeStationAPI.getCommonName();
        this.latitude = bikeStationAPI.getLat();
        this.longitude = bikeStationAPI.getLon();
        this.bikesNumber = bikesNumber;
        this.modified = bikeStationAPI.getModifiedDate();
        this.docksNumber = bikeStationAPI.getDocksNumber();
        this.emtyDocksNumber = bikeStationAPI.getEmptyDocksNumber();
    }

    /**
     * Get instance of BikeStation created from the data provided by BikeStationAPI.
     * @param bikeStationAPI
     * @return
     */
    public static BikeStation getInstance(BikeStationAPI bikeStationAPI) {
        return new BikeStation(bikeStationAPI, bikeStationAPI.getBikesNumber());
    }

    /**
     * Get instance of BikeStation from the data provided by BikeStationAPI.
     * Method allows of setting number of bikes independently from the data
     * provided by TfL API
     * @param bikeStationAPI
     * @param bikesNumber
     * @return
     */
    public static BikeStation getInstance(BikeStationAPI bikeStationAPI, int bikesNumber) {
        return new BikeStation(bikeStationAPI, bikesNumber);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    public int getBikesNumber() {
        return bikesNumber;
    }

    public int getDocksNumber() {
        return docksNumber;
    }

    public int getEmtyDocksNumber() {
        return emtyDocksNumber;
    }

    public String getModified() {
        return modified;
    }
}
