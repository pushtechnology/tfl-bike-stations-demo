/*******************************************************************************
 * Copyright (C) 2017 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package net.example.mkruglik.domain;


/**
 * Interface representing map location.
 * @author mkruglik
 *
 */
public interface Location {

    /**
     * Returns location's latitude.
     *
     * @return
     * @since n.n
     */
    double getLatitude();

    /**
     * Returns location's longitude.
     *
     * @return
     * @since n.n
     */
    double getLongitude();

    /**
     * Returns location's name.
     *
     * @return
     * @since n.n
     */
    String getName();

    /**
     * Returns location's ID.
     *
     * @return
     * @since n.n
     */
    String getId();
}
