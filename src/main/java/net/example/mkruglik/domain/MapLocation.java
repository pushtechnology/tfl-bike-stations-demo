/*******************************************************************************
 * Copyright (C) 2017 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.domain;

/**
 * Default implementation of map location (Location interface).
 * @author mkruglik
 *
 */
public class MapLocation implements Location {

    private final double latitude;
    private final double longitude;

    /**
     * Constructor. Creates instance of MapLocation with latitude and longitude
     * as parameters
     * @param latitude
     * @param longitude
     */
    public MapLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public double getLatitude() {
        return this.latitude;
    }

    @Override
    public double getLongitude() {
        return this.longitude;
    }

    /**
     * Returns location name in format "lat: (latitude), lon: (longitude)".
     */
    @Override
    public String getName() {
        return "lat: " + latitude + ", lon:" + longitude;
    }

    /**
     * Return location id in format "(latitude), (longitude)".
     */
    @Override
    public String getId() {
        return latitude + ", " + longitude;
    }

    @Override
    public String toString() {
        return "map location - " + getName();
    }
}
