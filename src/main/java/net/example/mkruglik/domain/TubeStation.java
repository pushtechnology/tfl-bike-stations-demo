/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

/**
 * Domain classes used to produce JSON representation of Locations.
 * @author mkruglik
 */
package net.example.mkruglik.domain;

import net.example.mkruglik.tflAPI.domain.TubeStationAPI;

/**
 * Class represents tube station with all details important from system point of view.
 * Normally it is used to rid of the unnecessary data provided by TfL API.
 * The class is immutable.
 *
 * @author mkruglik
 */
public final class TubeStation implements Location {
    private final String id;
    private final String name;
    private final double latitude;
    private final double longitude;

    private static TubeStation instance;

    private TubeStation(final TubeStationAPI tubeStationAPI) {
        this.name = tubeStationAPI.getName();
        this.id = tubeStationAPI.getId();
        this.latitude = tubeStationAPI.getLat();
        this.longitude = tubeStationAPI.getLon();
    }

    /**
     * Returns instance of TubeStation created from the TubeStationAPI instance.
     * @param tubeStationAPI
     * @return
     */
    public static TubeStation getInstance(final TubeStationAPI tubeStationAPI) {
        instance = new TubeStation(tubeStationAPI);
        return instance;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getLatitude() {
        return latitude;
    }

    @Override
    public double getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return "TubeStation [id=" + id + ", name=" + name + ", latitude=" +
            latitude + ", longitude=" + longitude + "]";
    }
}
