/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.publisher;

/**
 * Context used in diffusin Callback classes. It allows for getting additional
 * information from diffusion operations in form of string message.
 * @author mkruglik
 *
 */
public class CallbackContext {

    private String message;

    /**
     * Sets context's message.
     * <P>
     * This message can be used to pass information from the callback to other
     * parts of the software.
     *
     * @return
     * @since n.n
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Gets context's message.
     * <P>
     * This message can be used to pass information from the callback to other
     * parts of the software
     * @param message
     * @since n.n
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "CallbackContext [message=" + message + "]";
    }
}
