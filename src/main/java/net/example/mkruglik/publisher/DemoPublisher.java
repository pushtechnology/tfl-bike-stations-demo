/*******************************************************************************
 * Copyright (C) 2017 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package net.example.mkruglik.publisher;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.example.mkruglik.domain.BikeStation;
import net.example.mkruglik.domain.Station;
import net.example.mkruglik.publisher.exceptions.AddingTopicsFailedException;
import net.example.mkruglik.publisher.exceptions.UpdateTopicsFailedException;
import net.example.mkruglik.tflAPI.APIReader;
import net.example.mkruglik.tflAPI.Reader;
import net.example.mkruglik.tflAPI.domain.BikeStationAPI;

/**
 * Publisher client class which adds and update topics about tfl bike stations.
 * The data is provided by TfL public API whit changes regards to the available
 * bikes, dock stations and real updates to them (TfL API update bike stations
 * status every 5min)
 * @author mkruglik
 */
public final class DemoPublisher {
    private static final int UPDATE_INTERVALS = 1000 * 10 * 5;
    private static Client tflAPIClient;
    private static JSONPublishingClient publishingClient;
    private static CallbackContext context;
    private static Reader<BikeStationAPI[]> tflAPIReader;
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoPublisher.class);

    private DemoPublisher() {
    }

    /**
     * Execute the demo publisher application. Requires no parameters.
     *
     * @param args
     */
    //CHECKSTYLE:OFF: UncommentedMain
    public static void main(String[] args) {
        tflAPIClient = new ResteasyClientBuilder()
            .socketTimeout(10, TimeUnit.SECONDS)
            .establishConnectionTimeout(50, TimeUnit.SECONDS)
            .connectionPoolSize(4).build();
        publishingClient = JSONPublishingClient.getInstance();
        context = new CallbackContext();
        tflAPIReader = new APIReader<BikeStationAPI[]>(tflAPIClient, new BikeStationAPI[0]);

        try {
            BikeStationAPI[] bikeStationsAPI = getBikeStationsData();
            createBikeStationsTopicsTree(bikeStationsAPI);

            while (true) {
                // API data on TfL servers is updated every 5min for Bike Stations
                Thread.sleep(UPDATE_INTERVALS);
                bikeStationsAPI = getBikeStationsData();
                updateBikeStationTopicsTree(bikeStationsAPI);
            }
        }
        catch (AddingTopicsFailedException e) {
            LOGGER.info("problem with creating topic tree");
            e.printStackTrace();
        }
        catch (UpdateTopicsFailedException e) {
            LOGGER.info("problem during updating topics ");
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        finally {
            tflAPIClient.close();
            publishingClient.close();
        }
    }
   //CHECKSTYLE:ON: UncommentedMain

    private static void updateBikeStationTopicsTree(BikeStationAPI[] bikeStationsAPI)
        throws UpdateTopicsFailedException {
        for (BikeStationAPI bikeStationAPI : bikeStationsAPI) {
            final Station bikeStation = BikeStation.getInstance(bikeStationAPI);
            publishingClient.updateBikeStation(
                bikeStationAPI.getId(),
                bikeStation,
                context);
        }
    }

    private static BikeStationAPI[] getBikeStationsData() {
        return tflAPIReader.getEntity("BikePoint", Collections.emptyMap());
    }

    private static void createBikeStationsTopicsTree(BikeStationAPI[] bikeStationsAPI)
        throws AddingTopicsFailedException {
        for (BikeStationAPI bikeStationAPI : bikeStationsAPI) {
            final Station bikeStation = BikeStation.getInstance(bikeStationAPI);
            publishingClient.addBikeStation(
                bikeStationAPI.getId(),
                bikeStation,
                context);
        }
    }
}