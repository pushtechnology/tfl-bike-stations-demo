/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.publisher;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;
import com.fasterxml.jackson.dataformat.cbor.CBORGenerator;
import com.pushtechnology.diffusion.client.Diffusion;
import com.pushtechnology.diffusion.client.callbacks.ErrorReason;
import com.pushtechnology.diffusion.client.callbacks.Registration;
import com.pushtechnology.diffusion.client.callbacks.TopicTreeHandler;
import com.pushtechnology.diffusion.client.features.control.topics.TopicAddFailReason;
import com.pushtechnology.diffusion.client.features.control.topics.TopicControl;
import com.pushtechnology.diffusion.client.features.control.topics.TopicControl.AddContextCallback;
import com.pushtechnology.diffusion.client.features.control.topics.TopicControl.RemovalContextCallback;
import com.pushtechnology.diffusion.client.features.control.topics.TopicUpdateControl;
import com.pushtechnology.diffusion.client.features.control.topics.TopicUpdateControl.UpdateSource;
import com.pushtechnology.diffusion.client.features.control.topics.TopicUpdateControl.Updater;
import com.pushtechnology.diffusion.client.features.control.topics.TopicUpdateControl.Updater.UpdateContextCallback;
import com.pushtechnology.diffusion.client.session.Session;
import com.pushtechnology.diffusion.client.topics.details.TopicType;
import com.pushtechnology.diffusion.datatype.json.JSON;
import com.pushtechnology.diffusion.datatype.json.JSONDataType;

import net.example.mkruglik.domain.Station;
import net.example.mkruglik.publisher.exceptions.AddingTopicsFailedException;
import net.example.mkruglik.publisher.exceptions.RemovingTopicsFailedException;
import net.example.mkruglik.publisher.exceptions.UpdateTopicsFailedException;

/**
 * JSON diffusion client which controls and updates bike station topics.
 *
 * @author mkruglik - created 5 Jun 2017
 * @since n.n
 */
public final class JSONPublishingClient {
    private static final String ROOT_TOPIC = "topics/bike_stations/";
    private static final String SERVER_URL = "ws://localhost:8080";
    private final CBORFactory cborFactory = new CBORFactory();
    private final Session session;
    private final TopicControl topicControl;
    private final Logger logger =
        LoggerFactory.getLogger(JSONPublishingClient.class);

    private static JSONPublishingClient instance = new JSONPublishingClient();

    private volatile TopicUpdateControl.ValueUpdater<JSON> valueUpdater;
    private volatile Registration updateSourceRegistration;

    private JSONPublishingClient() {
        cborFactory.setCodec(new ObjectMapper());
        session = Diffusion.sessions().principal("client").password("password")
            .open(SERVER_URL);

        // create topic control handler
        topicControl = session.feature(TopicControl.class);
        topicControl
            .removeTopicsWithSession(ROOT_TOPIC, new TopicTreeHandler.Default());

        // create topic update handlers
        session.feature(TopicUpdateControl.class).registerUpdateSource(
            ROOT_TOPIC,
            new UpdateSource.Default() {

                @Override
                public void onRegistered(final String topicPath, final Registration registration) {
                    updateSourceRegistration = registration;
                }

                @Override
                public void onActive(final String topicPath, final Updater updater) {
                    valueUpdater = updater.valueUpdater(JSON.class);
                }
            });
   }

    /**
     * Singleton method returns instance of JSONPublishingClient.
     * @param serverUrl
     * @return
     */
    public static JSONPublishingClient getInstance() {
        return instance;
    }

    /**
     * Adds topic for new station returns callback context with success/fail
     * status of adding topic operation.
     * @param stationName
     * @param bikeStation
     * @param context
     * @return
     * @throws AddingTopicsFailedException
     */
    public CallbackContext addBikeStation(final String stationName,
        final Station bikeStation, final CallbackContext context)
            throws AddingTopicsFailedException {

        final CountDownLatch latch = getLatch(1);
        topicControl.addTopic(ROOT_TOPIC + stationName,
            TopicType.JSON,
            mapToJSON(bikeStation),
            context,
            new AddContextCallback<CallbackContext>() {

                @Override
                public void onDiscard(final CallbackContext context) {
                    latch.countDown();
                }

                @Override
                public void onTopicAdded(final CallbackContext context, final String topicPath) {
                    context.setMessage("topic created"); // status information about success
                    latch.countDown();
                }

                @Override
                public void onTopicAddFailed(final CallbackContext context, final String topicPath,
                    final TopicAddFailReason reason) {
                    context.setMessage("failed, topic " + reason);
                    latch.countDown();
                }
            });

        try {
            latch.await();
        }
        catch (final InterruptedException e) {
            throw new AddingTopicsFailedException(e);
        }
        return context;
    }

    /**
     * Removes topic from the topic tree.
     * @param stationName
     * @param context
     * @return
     * @throws RemovingTopicsFailedException
     */
    public CallbackContext remove(final String stationName, final CallbackContext context)
        throws RemovingTopicsFailedException {

        final CountDownLatch latch = getLatch(1);
        topicControl.remove(
            "?" + ROOT_TOPIC + stationName,
            context,
            new RemovalContextCallback<CallbackContext>() {

                @Override
                public void onError(final CallbackContext context,
                    final ErrorReason errorReason) {
                    context.setMessage("removal failed " + errorReason);
                    latch.countDown();
                }

                @Override
                public void onTopicsRemoved(final CallbackContext context) {
                    context.setMessage("topic removed");
                    latch.countDown();
                }
            });
        try {
            latch.await();
        }
        catch (final InterruptedException e) {
            throw new RemovingTopicsFailedException(e);
        }
        return context;
    }

    /**
     * Updates topic from topicPath. Returns callback context with success/fail
     * status of updating topic operation
     * @param stationName
     * @param bikeStation
     * @param context
     * @return
     * @throws UpdateTopicsFailedException
     */
    public CallbackContext updateBikeStation(final String stationName,
        final Station bikeStation, final CallbackContext context)
            throws UpdateTopicsFailedException {

        final CountDownLatch latch = getLatch(1);
        valueUpdater.update(
            ROOT_TOPIC + stationName,
            mapToJSON(bikeStation),
            context,
            new UpdateContextCallback<CallbackContext>() {
                @Override
                public void
                    onError(final CallbackContext context, final ErrorReason errorReason) {
                    context.setMessage("update failed, reason: " + errorReason);
                    latch.countDown();
                }

                @Override
                public void onSuccess(final CallbackContext context) {
                    context.setMessage("topic updated");
                    latch.countDown();
                }
            });
        try {
            latch.await();
        }
        catch (final InterruptedException e) {
            throw new UpdateTopicsFailedException(e);
        }
        return context;
    }

    /**
     * Maps list of bike stations to the JSON.
     * @param entityToMap
     * @return
     */
    private JSON mapToJSON(final Object entityToMap) {
        final JSONDataType jsonDataType = Diffusion.dataTypes().json();
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            final CBORGenerator generator = cborFactory.createGenerator(outputStream);
            generator.writeObject(entityToMap);
            return jsonDataType.readValue(outputStream.toByteArray());
        }
        catch (final IOException e) {
            logger.info("Problems with reading stream when mapping to JSON");
        }
        return jsonDataType.fromJsonString("{}");
    }

    /**
     * Returns COuntDownLatch required by other methods to make sure network
     * operation is completed before method process any further.
     * @param count
     * @return
     */
    private CountDownLatch getLatch(final int count) {
        return new CountDownLatch(count);
    }

    /**
     * Closes class resources.
     */
    public void close() {
        session.close();
        updateSourceRegistration.close();
    }

}
