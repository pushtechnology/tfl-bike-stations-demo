/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.publisher.exceptions;

/**
 * Exception thrown when topic creation has failed. It is rethrown to identify point
 * of failure in the JSONPublishingClient in place of more generic exception.
 *
 * @author mkruglik - created 5 Jun 2017
 */
public class AddingTopicsFailedException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new exception.
     * @param e
     */
    public AddingTopicsFailedException(Throwable e) {
        super(e);
    }

}
