/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.subscriber;

import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import com.pushtechnology.diffusion.client.Diffusion;
import com.pushtechnology.diffusion.client.session.Session;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import net.example.mkruglik.subscriber.util.TopicSubscriber;

/**
 * Subscriber application to display bike station nearby set location. Uses
 * javafx and GMapsFX framework for user interface.
 *
 * @author mkruglik - created 5 Jun 2017
 */
public class DemoSubscriber extends Application {

    private Client client;
    private GUIController controller;
    private final Session session = Diffusion.sessions().open("ws://localhost:8080");
    private SubscriberValueStream stream;

    @Override
    // initialise GUI application
    public void start(final Stage stage) throws Exception {
        client = new ResteasyClientBuilder()
            .socketTimeout(10, TimeUnit.SECONDS)
            .establishConnectionTimeout(50, TimeUnit.SECONDS)
            .connectionPoolSize(4).build();

        stream = new SubscriberValueStream();

        final FXMLLoader loader =
            new FXMLLoader(getClass().getResource("/Scene.fxml"));
        final Parent root = loader.load();
        controller = loader.<GUIController>getController();
        controller.setClient(client);

        final TopicSubscriber topicSubscriber = new TopicSubscriber(session, stream);
        controller.setStream(stream);
        controller.setTopicSubscriber(topicSubscriber);
        stream.setSceneController(controller);

        final Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }

    @Override
    // close GUI application
    public void stop() throws Exception {
        super.stop();
        client.close();
        controller.close();
    }
}
