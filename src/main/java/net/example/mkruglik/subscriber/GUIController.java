/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.subscriber;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.ws.rs.client.Client;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.javascript.event.GMapMouseEvent;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.GoogleMap;
import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.MapOptions;
import com.lynden.gmapsfx.javascript.object.MapTypeIdEnum;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import net.example.mkruglik.domain.BikeStation;
import net.example.mkruglik.domain.Location;
import net.example.mkruglik.domain.MapLocation;
import net.example.mkruglik.domain.TubeStation;
import net.example.mkruglik.subscriber.util.LocationIcon;
import net.example.mkruglik.subscriber.util.TopicSubscriber;
import net.example.mkruglik.tflAPI.TflAPIService;

/**
 * GUI controlling class responsible for displaying updates and stage creation.
 *
 * @author mkruglik - created 5 Jun 2017
 */
public class GUIController implements Initializable {

    private Client apiClient;
    private SubscriberValueStream stream;
    private TflAPIService tflAPIService;
    private TopicSubscriber topicSubscriber;

    @FXML
    private GoogleMapView googleMapView;
    @FXML
    private GridPane leftPane;
    @FXML
    private AnchorPane anchorPane;
    private TableView<TubeStation> tubeStationsTable;
    private Label stationBikeNumbers;
    private Label stationNameLabel;
    private Label stationEmptyDocks;
    private Label stationModifiedTimestamp;
    private VBox bikeStationDetails;

    private GoogleMap map;
    private final List<Marker> bikeMarkersCollections = new ArrayList<>();
    private Marker tubeMarker;
    private Marker mapLocationMarker;

    /**
     * Sets http client.
     * @param client
     */
    public void setClient(final Client client) {
        apiClient = client;
        tflAPIService = new TflAPIService(apiClient);
    }

    /**
     * Sets input stream.
     *
     * @param stream
     */
    public void setStream(final SubscriberValueStream stream) {
        this.stream = stream;
    }

    /**
     * Sets topic subscriber - diffusion client.
     *
     * @param subscriber
     */
    public void setTopicSubscriber(final TopicSubscriber subscriber) {
        topicSubscriber = subscriber;
    }

    @Override
    public void initialize(final URL url, final ResourceBundle rb) {

        final TextArea searchBox = new TextArea();
        searchBox.setPrefHeight(20);

        final Button searchButton = new Button("Search Tube Station");

        searchButton.setOnAction(v -> searchTube(searchBox));
        searchBox.setOnKeyPressed((final KeyEvent key) -> searchTube(searchBox, key));

        tubeStationsTable = new TableView<>();
        tubeStationsTable.setPrefHeight(200);
        tubeStationsTable.setVisible(false);

        stationNameLabel = new Label();
        stationBikeNumbers = new Label();
        stationEmptyDocks = new Label();
        stationModifiedTimestamp = new Label();
        bikeStationDetails = new VBox();
        bikeStationDetails.getChildren().addAll(stationNameLabel,
            stationBikeNumbers, stationEmptyDocks, stationModifiedTimestamp);
        bikeStationDetails.setVisible(false);

        leftPane.add(searchBox, 0, 0);
        leftPane.add(searchButton, 1, 0);
        leftPane.add(tubeStationsTable, 0, 1, 2, 1);
        leftPane.add(bikeStationDetails, 0, 2, 2, 1);

        googleMapView.addMapInializedListener(() -> configureMap());
    }

    private void searchTube(final TextArea searchBox, final KeyEvent key) {
        if (key.getCode() == KeyCode.ENTER) {
            key.consume();
            displayTubeStationTable(searchBox.getText());
            searchBox.clear();
        }
    }

    private void searchTube(final TextArea searchBox) {
        displayTubeStationTable(searchBox.getText());
    }

    private void displayTubeStationTable(final String query) {
        final List<TubeStation> tubeStations = tflAPIService.getTubeStations(query);

        tubeStationsTable.maxHeight(40);
        final ObservableList<TubeStation> observableList =
            FXCollections.observableArrayList();
        for (final TubeStation station : tubeStations) {
            observableList.add(station);
        }

        addObservableColumnToTable(tubeStationsTable);
        addListenersToTableRows(tubeStationsTable);

        tubeStationsTable.setItems(observableList);
        tubeStationsTable.setVisible(true);
    }

    private void addObservableColumnToTable(final TableView<TubeStation> tableView) {
        final TableColumn<TubeStation, String> stationNameCol =
            new TableColumn<>("Station name");
        stationNameCol.setCellValueFactory(
            new PropertyValueFactory<TubeStation, String>("name"));
        tableView.getColumns().add(stationNameCol);
    }

    private void addListenersToTableRows(final TableView<TubeStation> tableView) {
        tableView.setRowFactory(v -> {
            final TableRow<TubeStation> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 1 && (!row.isEmpty())) {
                    tubeStationTableRowClickHandle(row);
                }
            });
            return row;
        });
    }

    private void tubeStationTableRowClickHandle(final TableRow<TubeStation> row) {
        final TubeStation tubeStation = row.getItem();
        final double lat = tubeStation.getLatitude();
        final double lon = tubeStation.getLongitude();
        final List<BikeStation> bikeStations =
            tflAPIService.getBikeStations(lat, lon);
        topicSubscriber.unsubscribeFromTopics();
        topicSubscriber.subscribeToTopics(bikeStations);
        displayTubeMarkOnTheMap(tubeStation);
    }

    private void displayTubeMarkOnTheMap(final TubeStation tubeStation) {
        resetMapMarkers();
        // add marker for new location
        tubeMarker = addMarkerOnMap(tubeStation,
            LocationIcon.getDefaultIconPath().iconFactory("png"));
    }

    /**
     * Displays bikes stations lokations as a markers on the map.
     *
     * @param bikeStations
     */
    protected void
        displayBikesMarkOnTheMap(final Map<String, BikeStation> bikeStations) {
        map.removeMarkers(bikeMarkersCollections);
        bikeMarkersCollections.removeAll(bikeMarkersCollections);
        bikeStations.forEach((v, w) -> {
            final Marker marker = addMarkerOnMap(w,
                LocationIcon.getBikeStationIconPath(w).iconFactory("png"));
            bikeMarkersCollections.add(marker);
        });
    }

    private Marker addMarkerOnMap(final Location location, final String iconFactory) {
        final MarkerOptions options = new MarkerOptions();
        options.icon(iconFactory);
        final LatLong possition =
            new LatLong(location.getLatitude(), location.getLongitude());
        options.position(possition);
        options.visible(true);
        final Marker marker = new Marker(options);
        map.addMarker(marker);
        marker.setTitle(location.getId());
        if (location instanceof TubeStation) {
            map.setCenter(possition);
            map.setZoom(15); // numerical zoom value, might be worth to make it
                             // constant
        }
        else if (location instanceof BikeStation) {
            map.addUIEventHandler(marker, UIEventType.click,
                h -> handleBikeMarkerEvent(marker));
        }
        return marker;
    }

    private void handleBikeMarkerEvent(final Marker bikeMarker) {
        final BikeStation bikeStation =
            stream.getBikeStations().get(bikeMarker.getTitle());
        stationNameLabel.setText("Station Name: " + bikeStation.getName());
        stationBikeNumbers
            .setText("Number of bikes: " + bikeStation.getBikesNumber());
        stationEmptyDocks.setText(
            "Number of empty docks: " + bikeStation.getEmtyDocksNumber());
        stationModifiedTimestamp
            .setText("Modified: " + bikeStation.getModified());
        bikeStationDetails.setVisible(true);
    }

    private void configureMap() {
        final MapOptions mapOptions = new MapOptions();
        mapOptions.center(new LatLong(51.515037, -0.072384))
            .mapType(MapTypeIdEnum.ROADMAP)
            .zoom(9);

        googleMapView.setKey("AIzaSyBkfS3oZsMsoH0ZpMdRMCX7sr_Zjb4sVUI");
        map = googleMapView.createMap(mapOptions, false);

        map.addMouseEventHandler(UIEventType.click,
            (final GMapMouseEvent event) -> handleMapCLick(event));
    }

    private void handleMapCLick(final GMapMouseEvent event) {
        resetMapMarkers();

        final LatLong latLong = event.getLatLong();
        final double lat = latLong.getLatitude();
        final double lon = latLong.getLongitude();
        final Location location = new MapLocation(lat, lon);

        mapLocationMarker = addMarkerOnMap(location,
            LocationIcon.getDefaultIconPath().iconFactory("png"));
        final List<BikeStation> bikeStations =
            tflAPIService.getBikeStations(lat, lon);
        topicSubscriber.unsubscribeFromTopics();
        topicSubscriber.subscribeToTopics(bikeStations);
    }

    private void resetMapMarkers() {
        if (tubeMarker != null) {
            map.removeMarker(tubeMarker);
            tubeMarker = null;
        }
        if (mapLocationMarker != null) {
            map.removeMarker(mapLocationMarker);
            mapLocationMarker = null;
        }

        map.removeMarkers(bikeMarkersCollections);
        bikeMarkersCollections.removeAll(bikeMarkersCollections);
    }

    /**
     * Closes class resources.
     *
     * @since n.n
     */
    public void close() {
        tflAPIService.close();
    }
}