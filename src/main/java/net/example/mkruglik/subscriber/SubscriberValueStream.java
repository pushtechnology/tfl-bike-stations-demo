/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package net.example.mkruglik.subscriber;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;
import com.fasterxml.jackson.dataformat.cbor.CBORParser;
import com.pushtechnology.diffusion.client.content.Content;
import com.pushtechnology.diffusion.client.features.Topics.ValueStream;
import com.pushtechnology.diffusion.client.topics.details.TopicSpecification;

import javafx.application.Platform;
import net.example.mkruglik.domain.BikeStation;

/**
 * Input stream reader which parse JSON to POJO classes.
 *
 * @author mkruglik - created 5 Jun 2017
 */
public class SubscriberValueStream extends ValueStream.Default<Content> {

    private Map<String, BikeStation> bikeStations = new ConcurrentHashMap<>();
    private GUIController guiController;

    public void setSceneController(final GUIController sceneController) {
        this.guiController = sceneController;
    }

    public Map<String, BikeStation> getBikeStations() {
        return bikeStations;
    }

    /**
     * Clear map of bike stations by replacing it with empty map.
     *
     * @since n.n
     */
    public void removeBikeStations() {
        bikeStations = new ConcurrentHashMap<String, BikeStation>();
    }

    @Override
    public void onValue(
        final String topicPath,
        final TopicSpecification specification,
        final Content oldValue,
        final Content newValue) {
        final CBORFactory factory = new CBORFactory();
        try {
            final CBORParser parser =
                factory.createParser(newValue.asInputStream());
            final ObjectMapper objectMapper = new ObjectMapper();
            final BikeStation bikeStation =
                objectMapper.readValue(parser, BikeStation.class);
            getBikeStations().put(bikeStation.getId(), bikeStation);
            parser.close();

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    guiController.displayBikesMarkOnTheMap(getBikeStations());
                }
            });
        }
        catch (final IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClose() {
        super.onClose();
    }
}