/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.subscriber.util;

import com.lynden.gmapsfx.util.MarkerImageFactory;

import net.example.mkruglik.domain.BikeStation;

/**
 * Class responsible for creating icon factories used in map markers.
 *
 * @author mkruglik - created 5 Jun 2017
 * @since n.n
 */
public class LocationIcon {

    private static final String ALARM_ICON = "/img/places.png";
    private static final String WARNING_ICON = "/img/entertainment.png";
    private static final String OK_ICON = "/img/birds.png";
    private static final String DEFAULT_ICON = "/img/transport.png";

    private static LocationIcon instance;

    private String iconPath;

    /**
     * Gets location path for the marker icon with appropriate colour.
     *
     * @param bikeStation
     * @return
     * @since n.n
     */
    public static LocationIcon getBikeStationIconPath(final BikeStation bikeStation) {
        instance = new LocationIcon();
        final int bikesNumber = bikeStation.getBikesNumber();
        if (bikesNumber < 0) {
            throw new IllegalArgumentException(
                "number of bikes can't be less than 0 but is: " + bikesNumber);
        }
        else if (bikesNumber == 0) {
            instance.iconPath = ALARM_ICON;
            return instance;
        }
        else if (bikesNumber >= 1 && bikesNumber <= 10) {
            instance.iconPath = WARNING_ICON;
            return instance;
        }
        else {
            instance.iconPath = OK_ICON;
            return instance;
        }
    }

    /**
     * Gets location path for the default marker icon.
     *
     * @return
     * @since n.n
     */
    public static LocationIcon getDefaultIconPath() {
        instance = new LocationIcon();
        instance.iconPath = DEFAULT_ICON;
        return instance;
    }

    /**
     * Creates IconFactory for the map marker icon.
     *
     * @param type
     * @return
     * @since n.n
     */
    public String iconFactory(final String type) {
        return MarkerImageFactory.createMarkerImage(iconPath, type);
    }

    protected String getIconPath() {
        return this.iconPath;
    }
}
