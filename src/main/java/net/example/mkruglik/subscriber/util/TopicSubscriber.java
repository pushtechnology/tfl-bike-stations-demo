/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.subscriber.util;

import java.util.List;

import com.pushtechnology.diffusion.client.content.Content;
import com.pushtechnology.diffusion.client.features.Topics;
import com.pushtechnology.diffusion.client.session.Session;

import net.example.mkruglik.domain.BikeStation;
import net.example.mkruglik.subscriber.SubscriberValueStream;

/**
 * Topic subscriber which use diffusion session to listen and update bike
 * stations topics.
 *
 * @author mkruglik - created 5 Jun 2017
 */
public class TopicSubscriber {

    private static final String ROOT_TOPIC = "topics/bike_stations/";

    private final Session session;
    private final Topics topics;

    private final SubscriberValueStream stream;

    /**
     *
     * Constructor. Gets diffusion session and input data stream as parameters.
     *
     * @param session
     * @param stream
     */
    public TopicSubscriber(final Session session, final SubscriberValueStream stream) {
        this.session = session;
        this.stream = stream;
        topics = this.session.feature(Topics.class);
    }

    /**
     * Subscribes to the diffusion topics from the list.
     *
     * @param bikeStations
     */
    public void subscribeToTopics(final List<BikeStation> bikeStations) {
        for (final BikeStation bikeStation : bikeStations) {
            topics.addStream(
                "*" + ROOT_TOPIC + bikeStation.getId(),
                Content.class,
                stream);
            topics.subscribe("?" + ROOT_TOPIC + bikeStation.getId(),
                new Topics.CompletionCallback.Default());
        }
    }

    /**
     * Unsubscribes from all topics.
     *
     */
    public void unsubscribeFromTopics() {
        topics.unsubscribe("?" + ROOT_TOPIC + "//",
            new Topics.CompletionCallback.Default());
        stream.removeBikeStations();
    }

}
