/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.tflAPI;

import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 * Concrete implementation of Reader interface. It gets TfL API as a source of
 *
 * @author mkruglik - created 5 Jun 2017
 * @param <T>
 */
public class APIReader<T> implements Reader<T> {

    private static final String API_HOST = "https://api.tfl.gov.uk";
    private static final String APP_ID = "3f89ec6d";
    private static final String APP_KEY = "21b2dbb82f01bc792b54c5027cadb99c";

    private final Client client;
    private final WebTarget defaultTarget;
    private final T ref;

    /**
     * Constructs APiReader with http client as parameter. It takes reference
     * value of the returning type required for parsing JSON list by generic
     * method.
     * @param client
     * @param ref
     */
    public APIReader(final Client client, final T ref) {
        this.client = client;
        defaultTarget = this.client
            .target(API_HOST)
            .queryParam("app_id", APP_ID)
            .queryParam("app_key", APP_KEY);
        this.ref = ref;

    }

    @Override
    public Response getResponse(final String path, final Map<String, String> queryParams) {
        return getRequestInvocation(path, queryParams).invoke();
    }

    @SuppressWarnings("unchecked")
    @Override
    public T getEntity(final String path, final Map<String, String> queryParams) {
        final Invocation invocation = getRequestInvocation(path, queryParams);
        return (T) invocation.invoke(ref.getClass());
    }

    private Invocation getRequestInvocation(final String path,
        final Map<String, String> queryParams) {
        WebTarget target = defaultTarget;
        if (!"".equals(path) || path != null) {
            target = defaultTarget.path(path);
        }
        if (!queryParams.isEmpty() || queryParams != null) {
            for (final Map.Entry<String, String> entry : queryParams.entrySet()) {
                target = target.queryParam(entry.getKey(), entry.getValue());
            }
        }
        return target.request().buildGet();
    }

    @Override
    public void close() {
        client.close();
    }
}
