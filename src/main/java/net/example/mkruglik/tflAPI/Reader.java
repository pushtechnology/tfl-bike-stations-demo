/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.tflAPI;

import java.util.Map;

import javax.ws.rs.core.Response;

/**
 * Reader interface to get data for the system from remote resource.
 *
 * @author mkruglik - created 5 Jun 2017
 * @since n.n
 * @param <T>
 */
public interface Reader<T> {

    /**
     * Returns http response to network call.
     *
     * @param path
     * @param queryParams
     * @return
     * @since n.n
     */
    Response getResponse(String path, Map<String, String> queryParams);

    /**
     * Returns data entity class from remote call.
     *
     * @param path
     * @param queryParams
     * @return
     * @since n.n
     */
    T getEntity(String path, Map<String, String> queryParams);

    /**
     * Closes the reader and its resources.
     *
     * @since n.n
     */
    void close();

}
