/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.tflAPI;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.ws.rs.client.Client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.example.mkruglik.domain.BikeStation;
import net.example.mkruglik.domain.TubeStation;
import net.example.mkruglik.tflAPI.domain.BikeStationsByRadiusAPI;
import net.example.mkruglik.tflAPI.domain.TubeSearchResultAPI;

/**
 * Class which servees as a access point for TfL API reader and hides reader's
 * implementation.
 *
 * @author mkruglik - created 5 Jun 2017
 * @since n.n
 */
public class TflAPIService {

    private final ExecutorService executor =
        Executors.newSingleThreadExecutor();
    private final Client client;
    private static final Logger LOGGER = LoggerFactory.getLogger(TflAPIService.class);

    /**
     * Constructor. Gets http client as a parameter.
     *
     * @param client
     */
    public TflAPIService(final Client client) {
        this.client = client;
    }

    /**
     * Returns list of TubeStation(s) which match the query. It delegates search
     * to separate thread as it is a network operation.
     *
     * @param query
     * @return
     */
    public List<TubeStation> getTubeStations(final String query) {
        final CompletableFuture<List<TubeStation>> cf =
            new CompletableFuture<>();
        executor.execute(() -> {
            final Map<String, String> queryParams = new HashMap<String, String>();
            queryParams.put("query", query);
            queryParams.put("modes", "tube");
            final APIReader<TubeSearchResultAPI> reader =
                new APIReader<TubeSearchResultAPI>(
                    client,
                    new TubeSearchResultAPI());
            final TubeSearchResultAPI searchResult = reader.getEntity(
                "StopPoint/Search",
                queryParams);
            final List<TubeStation> tubeStations = new ArrayList<>();
            searchResult.getMatches().forEach(v ->
                tubeStations.add(TubeStation.getInstance(v)));
            cf.complete(tubeStations);
        });

        try {
            return cf.get(10, TimeUnit.SECONDS);
        }
        catch (InterruptedException | ExecutionException | TimeoutException e) {
            LOGGER.info("problem with tube station query from API");
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     * Returns list of BikeStation(s) in 500m radius from location (latitude,
     * longitude). It delegates search to separate thread as it is a network
     * operation.
     *
     * @param latitude
     * @param longitude
     * @return
     */
    public List<BikeStation> getBikeStations(
        final double latitude, final double longitude) {

        BikeStationsByRadiusAPI bikeStationsByRadiusAPI = null;

        final CompletableFuture<BikeStationsByRadiusAPI> cf =
            new CompletableFuture<>();
        executor.execute(() -> {
            final Map<String, String> queryParams =
                new HashMap<String, String>();
            queryParams.put("lat", Double.toString(latitude));
            queryParams.put("lon", Double.toString(longitude));
            queryParams.put("radius", "500");
            final APIReader<BikeStationsByRadiusAPI> reader =
                new APIReader<BikeStationsByRadiusAPI>(
                    client,
                    new BikeStationsByRadiusAPI());
            final BikeStationsByRadiusAPI entity = reader.getEntity(
                "BikePoint",
                queryParams);
            cf.complete(entity);
        });
        try {
            bikeStationsByRadiusAPI = cf.get();
            final List<BikeStation> bikeStations = new ArrayList<>();
            bikeStationsByRadiusAPI.getPlaces().forEach(v -> {
                bikeStations.add(BikeStation.getInstance(v));
            });
            return bikeStations;
        }
        catch (InterruptedException | ExecutionException e) {
            System.out
                .println("problem with reading BikeStations from tfl API");
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    /**
     * Closes class resources.
     *
     */
    public void close() {
        executor.shutdown();
    }
}
