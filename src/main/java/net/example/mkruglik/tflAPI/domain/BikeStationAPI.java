/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.tflAPI.domain;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Data holder class used for parsing JSON received from TfL API.
 *
 * @author mkruglik
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BikeStationAPI {

    private String id;
    private String url;
    private String commonName;
    private String placeType;
    private List<BikeStationPropertiesAPI> additionalProperties;
    private double lat;
    private double lon;

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getCommonName() {
        return commonName;
    }

    public String getPlaceType() {
        return placeType;
    }

    public List<BikeStationPropertiesAPI> getAdditionalProperties() {
        return additionalProperties;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    /**
     * Returns number of bikes in the station.
     *
     * @return
     */
    public int getBikesNumber() {
        final BikeStationPropertiesAPI bikesNumberProperty =
            getAdditionalProperty("NbBikes");
        return Integer.parseInt(bikesNumberProperty.getValue());
    }

    /**
     * Returns date of the last station's status update.
     *
     * @return
     */
    public String getModifiedDate() {
        final BikeStationPropertiesAPI bikesNumberProperty =
            getAdditionalProperty("NbBikes");
        return bikesNumberProperty.getModified().toString();
    }

    /**
     * Returns total number of docks in the station.
     * @return
     * @since n.n
     */
    public int getDocksNumber() {
        final BikeStationPropertiesAPI bikesNumberProperty =
            getAdditionalProperty("NbDocks");
        return Integer.parseInt(bikesNumberProperty.getValue());
    }

    /**
     * Returns number of empty docks in the station.
     * @return
     * @since n.n
     */
    public int getEmptyDocksNumber() {
        final BikeStationPropertiesAPI bikesNumberProperty =
            getAdditionalProperty("NbEmptyDocks");
        return Integer.parseInt(bikesNumberProperty.getValue());
    }

    private BikeStationPropertiesAPI getAdditionalProperty(final String key) {
        for (final BikeStationPropertiesAPI property : additionalProperties) {
            if (property.getKey().equals(key)) {
                return property;
            }
        }
        throw new IllegalArgumentException("No property with key " + key);
    }

    @Override
    public String toString() {
        return "id: " + id + ", url: " + url + ", name: " + commonName +
            ", number of property records: " + additionalProperties.size();
    }
}
