/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.tflAPI.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Holds properties data of the bike station.
 *
 * @author mkruglik - created 5 Jun 2017
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BikeStationPropertiesAPI {
    private String category;
    private String key;
    private String sourceSystemKey;
    private String value;
    private String modified;

    public String getCategory() {
        return category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }

    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public String getSourceSystemKey() {
        return sourceSystemKey;
    }

    public void setSourceSystemKey(final String sourceSystemKey) {
        this.sourceSystemKey = sourceSystemKey;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    /**
     * Returns date of last bike station data modification.
     *
     * @return
     */
    public LocalDateTime getModified() {
        final DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        final LocalDateTime modifiedDateTime =
            LocalDateTime.parse(modified, formatter);
        return modifiedDateTime;
    }

    public void setModified(final String modified) {
        this.modified = modified;
    }
}
