/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.tflAPI.domain;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Wrapper class which holds List of bike station by radius parsed from JSON.
 *
 * @author mkruglik - created 5 Jun 2017
 * @since n.n
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BikeStationsByRadiusAPI {

    private List<Double> centrePoint;
    private List<BikeStationAPI> places;

    public List<Double> getCentrePoint() {
        return centrePoint;
    }
    public void setCentrePoint(List<Double> centrePoint) {
        this.centrePoint = centrePoint;
    }
    public List<BikeStationAPI> getPlaces() {
        return places;
    }
    public void setPlaces(List<BikeStationAPI> places) {
        this.places = places;
    }
}
