/*******************************************************************************
 * Copyright (C) 2016 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.example.mkruglik.tflAPI.domain;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * POJO class to hold data of tube station parsed from TfL API JSON.
 *
 * @author mkruglik - created 5 Jun 2017
 * @since n.n
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TubeStationAPI {
    private long icsId;
    private String topMostParentId;
    private List<String> modes;
    private String id;
    private String name;
    private double lat;
    private double lon;

    public long getIcsId() {
        return icsId;
    }

    public String getTopMostParentId() {
        return topMostParentId;
    }

    public List<String> getModes() {
        return modes;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    @Override
    public String toString() {
        return "TubeStationAPI [icsId=" + icsId + ", topMostParentId=" +
            topMostParentId + ", modes=" + modes + ", id=" + id + ", name=" +
            name + ", lat=" + lat + ", lon=" + lon + "]";
    }
}