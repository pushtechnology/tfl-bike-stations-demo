package net.example.mkruglik.publisher;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import net.example.mkruglik.domain.Station;
import net.example.mkruglik.publisher.exceptions.AddingTopicsFailedException;
import net.example.mkruglik.publisher.exceptions.RemovingTopicsFailedException;
import net.example.mkruglik.publisher.exceptions.UpdateTopicsFailedException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PublisherTest {
    
    private static final String ANY_STATION_NAME = "Aldgate East";
    private static final Station BIKE_STATION = new TestBikeStation("test");
    
    private static JSONPublishingClient publisher;
    CallbackContext context;
    
    @BeforeClass
    public static void setUpClass() {
        publisher = JSONPublishingClient.getInstance();
    }
    
    @AfterClass
    public static void clean() {
        publisher.close();
    }

    @Before
    public void setUp() throws RemovingTopicsFailedException {
        context = new CallbackContext();
        publisher.remove("", context);
    }
    
    @Test
    public void testIfTopicIsAdded() throws InterruptedException, Exception, IOException {
        context = publisher.addBikeStation(ANY_STATION_NAME, BIKE_STATION, context);

        assertEquals("topic created", context.getMessage());
    }

    @Test
    public void testIfTopicIsRemoved() throws AddingTopicsFailedException, RemovingTopicsFailedException  {
        publisher.addBikeStation(ANY_STATION_NAME, BIKE_STATION, context);
        
        context = publisher.remove(ANY_STATION_NAME, context);
        
        assertEquals("topic removed", context.getMessage());
    }
    
    
    // Test will fail if other publisher is registered for the topic as exclusive updater, e.g.: instance of DemoPublisher
    @Test
    public void testIfTopicIsUpdated() throws AddingTopicsFailedException, UpdateTopicsFailedException  {
       publisher.addBikeStation(ANY_STATION_NAME, BIKE_STATION, context);

       Station bikeStation1 = new TestBikeStation("some name");

       context = publisher.updateBikeStation(ANY_STATION_NAME, bikeStation1, context);

       assertEquals("topic updated", context.getMessage());
    }
}
