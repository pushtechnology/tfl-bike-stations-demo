package net.example.mkruglik.publisher;

import net.example.mkruglik.domain.Station;

public class TestBikeStation implements Station {
    
    private String name;
    
    public TestBikeStation() {
        this("default name");
    }

    public TestBikeStation(String name) {
        this.name = name;
    }
    
    @Override
    public String getName() {
        return name;
    }

}
