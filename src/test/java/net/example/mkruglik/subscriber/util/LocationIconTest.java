package net.example.mkruglik.subscriber.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import net.example.mkruglik.domain.BikeStation;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;



@RunWith(JUnitParamsRunner.class)
public class LocationIconTest {

    @Rule
    public MockitoRule MockitoRule = MockitoJUnit.rule();
    
    @Test
    public void shouldReturnAlarmIconIfNoBikes() {
        BikeStation bikeStation = mock(BikeStation.class);
        when(bikeStation.getBikesNumber()).thenReturn(0);
        
        String iconPath = LocationIcon.getBikeStationIconPath(bikeStation).getIconPath();
        
        assertEquals("/places.png", iconPath);
    
    }
    
    public Object[] getWarningBikesNumber() {
        return new Integer[] {1, 6, 9, 10}; 
    }
    
    @Test
    @Parameters(method = "getWarningBikesNumber")
    public void shouldReturnWarningIconIfBikesBetweenOneAndTen(int bikesNumber) {
        BikeStation bikeStation = mock(BikeStation.class);
        when(bikeStation.getBikesNumber()).thenReturn(bikesNumber);
        
        String iconPath = LocationIcon.getBikeStationIconPath(bikeStation).getIconPath();

        assertEquals("/entertainment.png", iconPath);
    }

    public Object[] getOKBikesNumber() {
        return new Integer[] {11, 26, 39}; 
    }
    
    @Test
    @Parameters(method = "getOKBikesNumber")
    public void shouldReturnOKIconIfMoreThanTen(int bikesNumber) {
        BikeStation bikeStation = mock(BikeStation.class);
        when(bikeStation.getBikesNumber()).thenReturn(bikesNumber);
        
        String iconPath = LocationIcon.getBikeStationIconPath(bikeStation).getIconPath();
        
        assertEquals("/birds.png", iconPath);
    }
    
    public Object[] getIllegalBikesNumber() {
        return new Integer[] {-1, -193, -12, -3}; 
    }
    
    @Test(expected = IllegalArgumentException.class)
    @Parameters(method = "getIllegalBikesNumber")
    public void shouldThrowExceptionIfBikesNUmberLessThanZero(int bikesNumber) {
        BikeStation bikeStation = mock(BikeStation.class);
        when(bikeStation.getBikesNumber()).thenReturn(bikesNumber);
        
        LocationIcon.getBikeStationIconPath(bikeStation);
        
    }
    
    @Test
    public void shouldReturnTubeStationIcon() {
        String iconPath = LocationIcon.getDefaultIconPath().getIconPath();
        assertEquals("/transport.png", iconPath);
    }
}
