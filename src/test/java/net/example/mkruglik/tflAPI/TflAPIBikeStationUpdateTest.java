package net.example.mkruglik.tflAPI;


import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.example.mkruglik.tflAPI.domain.BikeStationAPI;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.junit.Test;


public class TflAPIBikeStationUpdateTest {

    private static Client CLIENT = new ResteasyClientBuilder()
    .socketTimeout(10, TimeUnit.SECONDS)
    .establishConnectionTimeout(50, TimeUnit.SECONDS)
    .connectionPoolSize(4).build();
    private static final String APP_ID = "3f89ec6d";
    private static final String APP_KEY = "21b2dbb82f01bc792b54c5027cadb99c";
    
    @Test
    public void testFrequencyOfUpdate() throws InterruptedException {
        String bikeStationId = "BikePoints_83";
        LocalDateTime oldTimestamp = null;
        int counter = 0;
        Invocation clientInvocation = CLIENT
            .target("https://api.tfl.gov.uk/BikePoint/" + bikeStationId)
            .queryParam("app_id", APP_ID)
            .queryParam("app_key", APP_KEY)
            .request(MediaType.APPLICATION_JSON)
            .buildGet();
        while(true) {
            System.out.print((++counter) + ": ");
            Response clientResponse = clientInvocation.invoke();        
            BikeStationAPI bikeStationResponse = clientResponse.readEntity(BikeStationAPI.class);
            LocalDateTime newTimestamp = bikeStationResponse.getAdditionalProperties().get(1).getModified();
            if (oldTimestamp == null || !newTimestamp.equals(oldTimestamp)) {
                oldTimestamp = newTimestamp;
                System.out.println(oldTimestamp);
            }
            clientResponse.close();
            Thread.sleep(5 * 60 * 1000);
        }
    }}
