package net.example.mkruglik.tflAPI;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;

import net.example.mkruglik.tflAPI.domain.BikeStationsByRadiusAPI;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.junit.BeforeClass;
import org.junit.Test;

public class TflAPIReaderTest {

    private static Client CLIENT;

    @BeforeClass
    public static void setUp() {
        CLIENT = new ResteasyClientBuilder()
        .socketTimeout(10, TimeUnit.SECONDS)
        .establishConnectionTimeout(50, TimeUnit.SECONDS)
        .connectionPoolSize(4).build();
    }
    
    @Test
    public void testIfReaderConnectsWithAPI() {
        String path = "BikePoint";
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("lat", "51.515037");
        queryParams.put("lon", "-0.072384");
        queryParams.put("radius", "200");
        
        Reader<String> reader = new APIReader<>(CLIENT, new String());
        Response response = reader.getResponse(path, queryParams);
        assertEquals(200, response.getStatus());
    }
    
    @Test
    public void testIfReaderReturnsBikeStationsByRadius() {
        String path = "BikePoint";
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("lat", "51.515037");
        queryParams.put("lon", "-0.072384");
        queryParams.put("radius", "200");
        Reader<BikeStationsByRadiusAPI> reader = new APIReader<BikeStationsByRadiusAPI>(CLIENT, new BikeStationsByRadiusAPI());
        BikeStationsByRadiusAPI bikeStations = reader.getEntity(path, queryParams);
        
       assertNotNull(bikeStations);
       assertEquals(51.515037, bikeStations.getCentrePoint().get(0), 0.0001);
       assertNotNull(bikeStations.getPlaces());
    }

}
