package net.example.mkruglik.tflAPI;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import net.example.mkruglik.tflAPI.domain.BikeStationsByRadiusAPI;
import net.example.mkruglik.tflAPI.domain.TubeSearchResultAPI;
import net.example.mkruglik.tflAPI.domain.TubeStationAPI;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class TflAPITEst {
    private static Client CLIENT;
    private static WebTarget DEFAULT_WEBTARGET;
    private static final String API_HOST = "https://api.tfl.gov.uk";
    private static final String APP_ID = "3f89ec6d";
    private static final String APP_KEY = "21b2dbb82f01bc792b54c5027cadb99c";

    @BeforeClass
    public static void setUp() {
        CLIENT = new ResteasyClientBuilder()
            .socketTimeout(10, TimeUnit.SECONDS)
            .establishConnectionTimeout(50, TimeUnit.SECONDS)
            .connectionPoolSize(4).build();
        DEFAULT_WEBTARGET = CLIENT
            .target(API_HOST)
            .queryParam("app_id", APP_ID)
            .queryParam("app_key", APP_KEY);
    }

    @AfterClass
    public static void close() {
        CLIENT.close();
    }

    @Test
    public void testIfClientsReceiveResponseFromServer()
        throws InterruptedException {
        Invocation clientInvocation = DEFAULT_WEBTARGET
            .path("BikePoint")
            .queryParam("lat", "51.515037")
            .queryParam("lon", "-0.072384")
            .queryParam("radius", 200) // in meters
            .request().buildGet();
        assertTrue(clientInvocation.invoke().getStatus() == 200);
        /*for (int i = 1; i < 10; i++) {
            System.out.println(i);
            Response response = clientInvocation.invoke();
            response.close();
        }*/
    }

    @Test
    public void testIfSearchOfTubeStationsByNameReturnsResults()
        throws InterruptedException {
        String query = "Picca";
        Invocation clientInvocation = DEFAULT_WEBTARGET
            .path("StopPoint/Search")
            .queryParam("query", query)
            .queryParam("modes", "tube")
            .request().buildGet();

        Response clientResponse = clientInvocation.invoke();
        TubeSearchResultAPI queryResult =
            clientResponse.readEntity(TubeSearchResultAPI.class);
        assertTrue(clientResponse.getStatus() == 200);
        assertNotNull(queryResult);
        assertTrue(queryResult.getMatches().size() > 0);
        TubeStationAPI tubeSearchResults = queryResult.getMatches().get(0);
        assertTrue(tubeSearchResults.getName().contains(query));
        clientResponse.close();
    }
    
    @Test
    public void checkIfBikeStationsAreReturnedFromRadiusQuery() {
        String tubeStationSearchQuery = "Picca";
        Invocation clientTubeStationSearchInvocation = DEFAULT_WEBTARGET
            .path("StopPoint/Search")
            .queryParam("query", tubeStationSearchQuery)
            .queryParam("modes", "tube")
            .request().buildGet();

        Response clientTubeStationSearchResponse = clientTubeStationSearchInvocation.invoke();
        TubeSearchResultAPI queryResult =
            clientTubeStationSearchResponse.readEntity(TubeSearchResultAPI.class);
        clientTubeStationSearchResponse.close();
        if (queryResult.getMatches().size() > 0) {
            TubeStationAPI tubeSearchResults = queryResult.getMatches().get(0);
            double lat = tubeSearchResults.getLat();
            double lon = tubeSearchResults.getLon();
            
           
            
            Invocation clientBikeStationsByRadiusInvocation = DEFAULT_WEBTARGET.path("BikePoint")
                .queryParam("lat", lat)
                .queryParam("lon", lon)
                .queryParam("radius", 200) // radius in meters
                .request().buildGet();
            Response clientBikeStationsResponse = clientBikeStationsByRadiusInvocation.invoke();
            BikeStationsByRadiusAPI bikeStationsByRadius = clientBikeStationsResponse.readEntity(BikeStationsByRadiusAPI.class);
            assertNotNull(bikeStationsByRadius);
            assertTrue(bikeStationsByRadius.getCentrePoint().size() > 0);
            assertTrue(bikeStationsByRadius.getPlaces().size() > 0);
            clientBikeStationsResponse.close();
            
        }
    }
}
